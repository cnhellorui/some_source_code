package io;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * <p>description: 一切从简的原则，尽可能显露出ibatis处理xml的骨架，提出那些额外的处理部分。
 *  只保留io读取和xml如何转换的东西。</p>
 *
 * @author chenrui
 * @since 2019-04-08
 */
public class Resource {


    private Resource() {
    }

    /**
     * 根据文件的路径读取对应的文件，返回对应的数据流
     * @param resource 文件的路径
     * @return
     * @throws IOException
     */
    public static InputStream getResourceAsStream(String resource) throws IOException {
        InputStream in = null;
        if (in == null)
            in = ClassLoader.getSystemResourceAsStream(resource);
        if (in == null)
            throw new IOException("Could not find resource " + resource);
        return in;
    }

}
