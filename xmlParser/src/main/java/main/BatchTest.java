package main;

import io.Resource;
import parser.SqlMapConfigParser;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

/**
 * <p>description: </p>
 *
 * @author chenrui
 * @since 2019-04-08
 */
public class BatchTest {

    public static void main(String[] args) throws IOException {
        String resours = "SqlMapConfig.xml";
        InputStream inputStream = Resource.getResourceAsStream(resours);

        new SqlMapConfigParser().parse(inputStream);
    }

}
