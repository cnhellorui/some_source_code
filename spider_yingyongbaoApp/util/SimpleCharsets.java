package util;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public final class SimpleCharsets {

    /**
     * Seven-bit ASCII, a.k.a. ISO646-US, a.k.a. the Basic Latin block of the
     * Unicode character set
     */
	public static final Charset US_ASCII = StandardCharsets.US_ASCII;
    /**
     * ISO Latin Alphabet No. 1, a.k.a. ISO-LATIN-1
     */
    public static final Charset ISO_8859_1 = StandardCharsets.ISO_8859_1;
    /**
     * Eight-bit UCS Transformation Format
     */
    public static final Charset UTF_8 = StandardCharsets.UTF_8;
    /**
     * Sixteen-bit UCS Transformation Format, big-endian byte order
     */
    public static final Charset UTF_16BE = StandardCharsets.UTF_16BE;
    /**
     * Sixteen-bit UCS Transformation Format, little-endian byte order
     */
    public static final Charset UTF_16LE = StandardCharsets.UTF_16LE;
    /**
     * Sixteen-bit UCS Transformation Format, byte order identified by an
     * optional byte-order mark
     */
    public static final Charset UTF_16 = StandardCharsets.UTF_16;
    /**
     * UTF_32, big-endian byte order
     */
    public static final Charset UTF_32BE = Charset.forName("UTF-32BE");
    /**
     * UTF_32, little-endian byte order
     */
    public static final Charset UTF_32LE = Charset.forName("UTF-32LE");
    /**
     * 原本ISO 10646标准定义了一个32位元的编码形式，称作UCS-4，使用通用字符集（UCS）的每一个字符，
     * 会在0到十六进制的7FFFFFFF这样的字码空间中，被表示成一个的32位元的码值。
     * <br/><br/>
     * UCS-4足以用来表示所有的Unicode的字码空间，其最大的码位为十六进制的10FFFF，所以其空间约有百万个码位。
     * 有些人认为保留如此大的字码空间却只为了对应这很小的码集是浪费的所以一个新的编码UTF-32被提出来了。
     * UTF-32 是一个 UCS-4 的子集，使用32-位元的码值，只在0到10FFFF的字码空间。
     * <br/><br/>
     * UTF-32 原本是 UCS-4 的子集，但JTC1/SC2/WG2声明，所有未来对字符的指定都将会限制在BMP及其14个补充平面，
     * 并移除先前在 E0 到 FF 平面的 60 到 7F 群的私用空间。
     * <br/><br/>
     * 于是就现状而言，除了 UTF-32 标准包含额外的 Unicode 意涵，UCS-4 和 UTF-32 大体是相同的。
     * <br/><br/>
     * byte order identified by an optional byte-order mark
     */
    public static final Charset UTF_32 = Charset.forName("UTF-32");
    /**
     * 双字节编码方案，其编码范围从8140至FEFE（剔除xx7F），<br/>
     * 共23940个码位，共收录了21003个汉字，完全兼容GB2312-80标准，<br/>
     * 支持国际标准ISO/IEC10646-1和国家标准GB13000-1中的全部中日韩汉字，并包含了BIG5编码中的所有汉字。
     */
    public static final Charset GBK = Charset.forName("GBK");
    /**
     * 国家标准GB18030-2005《信息技术 中文编码字符集》是我国继GB2312-1980和GB13000.1-1993之后最重要的汉字编码标准，<br/>
     * 是我国计算机系统必须遵循的基础性标准之一。 <br/>
     * GB18030有两个版本：
     * <ul>
     * <li>GB18030-2000和GB18030-2005。GB18030-2000是GBK的取代版本，它的主要特点是在GBK基础上增加了CJK统一汉字扩充A的汉字。<br/>
     * GB18030-2000编码标准是由信息产业部和国家质量技术监督局在2000年 3月17日联合发布的，并且将作为一项国家标准在2001年的1月正式强制执行。<br/>
     * </li>
     * <li>
     * GB18030-2005的主要特点是在GB18030-2000基础上增加了CJK统一汉字扩充B的汉字。<br/>
     * GB18030-2005《信息技术中文编码字符集》是我国自主研制的以汉字为主并包含多种我国少数民族文字（如藏、蒙古、傣、彝、朝鲜、维吾尔文等）的超大型中文编码字符集强制性标准，其中收入汉字70000余个。
     * </li>
     * </ul>
     */
    public static final Charset GB18030 = Charset.forName("GB18030");
    /**
     * GB2312标准共收录6763个汉字，其中一级汉字3755个，二级汉字3008个；<br/>
     * 同时，GB 2312收录了包括拉丁字母、希腊字母、日文平假名及片假名字母、俄语西里尔字母在内的682个全角字符。<br/>
     * 对于人名、古汉语等方面出现的罕用字，GB2312不能处理，这导致了后来GBK及GB18030汉字字符集的出现。
     */
    public static final Charset GB2312 = Charset.forName("GB2312");
}
