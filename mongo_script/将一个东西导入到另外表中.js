var find = db.getCollection("houhou").find({});
find.forEach(function(event) {

    var flag = true;
    db.memeda.find({"category": "娱乐/音乐/歌名", "word":event.song_name}).forEach(function(n) {
        flag = false;
    });
    if(flag) {
        print(event.song_name);
        var lexiconSong = {};

        var category = [];
        var synonyms = [];
        var word;
        var variables = {};
        var tags = [];

        category.push("娱乐/音乐/歌名");
        lexiconSong.category = category;

        synonyms.push(event.song_name);
        if(event.song_name != event.song_orig) {
            synonyms.push(event.song_orig);
        }
        lexiconSong.synonyms = synonyms;

        word = event.song_name;
        lexiconSong.word = word;

        variables.song_mid = event.song_mid;
        variables.singer_mid = event.singer[0].mid;
        variables["中文"] = event.album.name;
        variables["日本"] = event.singer[0].name;
        lexiconSong.variables = variables;

        lexiconSong.tags = tags;

        db.getCollection("memeda").insert(lexiconSong);
    }
})