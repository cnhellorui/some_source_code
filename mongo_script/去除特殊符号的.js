var arr = [
	{"word":"初与元九别后忽梦见之。及寤而书适……","newWord":""},
{"word":"初除主客郎中知制诰，与王十一……","newWord":""},
{"word":"酬微之开拆新楼初毕相报末联见……","newWord":"酬微之开拆新楼初毕相报末联见戏之作"},
{"word":"酬梦得以予五月长斋延僧徒绝宾友见……","newWord":"酬梦得以予五月长斋延僧徒绝宾友见戏十韵"},
{"word":"崔侍御以孩子三日示其所生诗见……","newWord":"崔侍御以孩子三日示其所生诗见示因以二绝句和之"}
];

for(var i=0; i<arr.length; i++) {
	
	var word = arr[i].word;
	var newWord = arr[i].newWord;
	
	if(newWord) {
		db.getCollection("memeda").find({"word":word}).forEach(function(event) {
			print(event.word + "	"+event._id);
			var synonyms = new Array();
			synonyms = event.synonyms;
			synonyms.push(newWord);
			
			db.getCollection("memeda").update({"_id":event._id}, {$set:{"word":newWord,"synonyms":synonyms }});
			
		})
	}
}
