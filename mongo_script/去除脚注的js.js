var find = db.getCollection("memeda").find({"category": "地点"}).limit(1);

find.forEach(function (event) {
    var match = false;
    for (var item in event.variables) {
        var str = event.variables[item];

        if (typeof str == "string" && /\[\d+\-?\d*\]/.test(str)) {
            match = true;
            var name = str.replace(/\[\d+\-?\d*\]/g, "");

            event.variables[item] = name;
            print(event.variables[item] + " -- > " + str + " --> " + name);
        }
    }

    if (match) {
        db.getCollection("memeda").updateOne({"_id": event._id}, {$set: {"variables": event.variables}});
    }
});
//str.replace(/\[\d+\-?\d*\]/g, "$1");
