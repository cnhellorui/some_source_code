var find = db.getCollection("memeda").find({ $or: [ { "language": { $in: [NumberInt(1), NumberInt(0)] } }, { "language": { $exists: false } } ] });

find.forEach(function(houhou) {

    var existFlag = true;
    db.getCollection("memeda").find({ "variables.song_mid" : houhou._id}).forEach(function( event) {
        existFlag=false;
    });

    if(existFlag) {
        //lexicon中如果不存在，讲qq.song格式化存放到lexicon中
        //houhou
        var lexiconSong = new Object();


        var category = new Array();
        category.push("娱乐/音乐/歌名");
        lexiconSong.category = category;

        var synonyms = new Array();
        synonyms.push(houhou.song_name);
        synonyms.push(houhou.song_orig);
        lexiconSong.synonyms = synonyms;

        lexiconSong.word = houhou.song_name;

        var variables = new Object();
        variables.song_mid = houhou._id;
        if(houhou.song_mid) {
            variables.singer_mid = houhou.song_mid;
            variables['歌手'] = houhou.singer[0].name;
        }
        if(houhou.album.name) {
            variables['专辑'] = houhou.album.name;
        }
        lexiconSong.variables = variables;

        var tags = new Array();
        tags.push("中文");

        lexiconSong.tags = tags;

        db.getCollection("memeda.new").save(lexiconSong);
        print(houhou._id + "	" + houhou.song_name);
        //print(houhou._id + "	" + houhou.song_name);
    }

});



