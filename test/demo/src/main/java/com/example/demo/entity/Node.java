package com.example.demo.entity;


public class Node {

    /**
     * 下一跳
     */
    private String ip;

    /**
     * 权重
     */
    private int weight;

    /**
     * 距离
     */
    private String distence;

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getDistence() {
        return distence;
    }

    public void setDistence(String distence) {
        this.distence = distence;
    }
}
