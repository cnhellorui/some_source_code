package com.example.demo.timer;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class TimerUpdate {

    @Scheduled(cron = "0/10 * * * * ?")
    public void timerUpdate() {
        System.out.println("你好，定时任务");
    }

}
