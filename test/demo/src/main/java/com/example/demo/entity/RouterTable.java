package com.example.demo.entity;

import java.util.LinkedHashMap;
import java.util.Map;

public class RouterTable {

    private Map<String, Node> routerMap = new LinkedHashMap<>();

    public Map<String, Node> getRouterMap() {
        return routerMap;
    }

    public void setRouterMap(Map<String, Node> routerMap) {
        this.routerMap = routerMap;
    }
}
