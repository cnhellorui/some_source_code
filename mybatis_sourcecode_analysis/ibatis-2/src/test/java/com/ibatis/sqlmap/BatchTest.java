/**
 * Copyright 2004-2019 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ibatis.sqlmap;

import com.ibatis.sqlmap.engine.execution.BatchException;
import com.ibatis.sqlmap.engine.execution.BatchResult;
import testdomain.Account;

import java.sql.BatchUpdateException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jeff Butler
 *
 */
public class BatchTest extends BaseSqlMapTest {

  @Override
  protected void setUp() throws Exception {
    //加载SqlMapConfig配置文件，将配置生成在系统中
    initSqlMap("com/ibatis/sqlmap/maps/SqlMapConfig.xml", null);
    //加载对应的sql文件
    //initScript("scripts/account-init.sql");
  }

  /**
   * @TODO
   *
   * 源码入口，这里是可以执行成功的
   * 笔记：题目：sqlMapConfigParser 和 NodeleteParser 和 Nodelete 接口的关系。
   *
   *  明白一点，sqlMapConfigParser 的构造方法往letMap中添加Nodelet初试化的对象
   *  同时Nodelet初试化的对象对象后，对象里的方法不首先执行的，只有认有调用后才执行
   *  NodeletParser中的letMap的map中存放着key:xpath和value:Nodelete对象，
   *  比对letMap中的key后调用Nodelete的process就能调用对象的process方法了。
   *
   *  这里就可以为接口的多态应用
   *    都是同一个Nodelete接口，Nodelete.process进入不同实现类中的process方法。
   *    同时new Nodelete()已经构造除了对象了，无需再进行new了。
   *
   * 笔记：题目：XmlParserState是干什么的。
   *  sqlMapConfigParser 和 NodeleteParser 和 Nodelete 最终目的是读取SqlMapConfig.xml文件，然后把文件的内容
   *  灌到 XmlParserState 对象中去，然后拿着 XmlParserState 让他去调用sql执行，缓存，返回结果映射，事务管理。
   *  理解了先后关系，这样一步一步就可以很好的分析代码了。
   */
  public void testExecutebatchDetailed() {
    List accountList1 = new ArrayList();
    Account account = new Account();
    account.setId(10);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(11);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(12);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(13);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(14);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    List accountList2 = new ArrayList();
    account = new Account();
    account.setId(15);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    account = new Account();
    account.setId(16);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    account = new Account();
    account.setId(17);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    account = new Account();
    account.setId(18);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    try {
      sqlMap.startTransaction();
      sqlMap.startBatch();

      // insert 5 accounts
      for (int i = 0; i < accountList1.size(); i++) {
        sqlMap.insert("insertAccountViaInlineParameters", accountList1.get(i));
      }

      // update 1 account
      account = new Account();
      account.setId(10);
      account.setFirstName("barney");
      account.setLastName("rubble");
      account.setEmailAddress("barney.rubble@gmail.com");

      sqlMap.update("updateAccountViaInlineParameters", account);

      // insert 4 accounts
      for (int i = 0; i < accountList2.size(); i++) {
        sqlMap.insert("insertAccountViaInlineParameters", accountList2.get(i));
      }

      List results = sqlMap.executeBatchDetailed();
      sqlMap.commitTransaction();

      assertEquals(3, results.size());

      BatchResult br = (BatchResult) results.get(0);
      assertEquals(5, br.getUpdateCounts().length);

      br = (BatchResult) results.get(1);
      assertEquals(1, br.getUpdateCounts().length);

      br = (BatchResult) results.get(2);
      assertEquals(4, br.getUpdateCounts().length);

    } catch (BatchException e) {
      fail(e.getMessage());
    } catch (SQLException e) {
      fail(e.getMessage());
    } finally {
      try {
        sqlMap.endTransaction();
      } catch (SQLException e) {
        fail(e.getMessage());
      }
    }
  }

  public void testExecutebatchDetailedWithError() {
    List accountList1 = new ArrayList();
    Account account = new Account();
    account.setId(10);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(11);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(12);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(13);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(14);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    List accountList2 = new ArrayList();
    account = new Account();
    account.setId(15);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    account = new Account();
    account.setId(16);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    account = new Account();
    account.setId(17);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    account = new Account();
    account.setId(18);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    try {
      sqlMap.startTransaction();
      sqlMap.startBatch();

      // insert 5 accounts
      for (int i = 0; i < accountList1.size(); i++) {
        sqlMap.insert("insertAccountViaInlineParameters", accountList1.get(i));
      }

      // update 1 account
      account = new Account();
      account.setId(10);
      account.setFirstName("barney");
      account.setLastName("rubble");
      account.setEmailAddress("barney.rubble@gmail.com");

      sqlMap.update("updateAccountViaInlineParameters", account);

      // insert another account
      account = new Account();
      account.setId(19);
      account.setFirstName("fred");
      account.setLastName("flintstone");
      account.setEmailAddress("fred.flintstone@gmail.com");
      sqlMap.insert("insertAccountViaInlineParameters", account);

      // insert 1 account with all null values (this should cause an error when the batch is
      // executed)
      account = new Account();
      sqlMap.insert("insertAccountViaInlineParameters", account);

      // update 1 account
      account = new Account();
      account.setId(11);
      account.setFirstName("barney");
      account.setLastName("rubble");
      account.setEmailAddress("barney.rubble@gmail.com");

      sqlMap.update("updateAccountViaInlineParameters", account);

      // insert 4 accounts
      for (int i = 0; i < accountList2.size(); i++) {
        sqlMap.insert("insertAccountViaInlineParameters", accountList2.get(i));
      }

      sqlMap.executeBatchDetailed();
      fail("This statement should not get executed - we expect an SQLException");
    } catch (BatchException e) {
      // the first statement of the failing batch should have executed OK
      BatchUpdateException bue = e.getBatchUpdateException();
      assertEquals(1, bue.getUpdateCounts().length);

      List results = e.getSuccessfulBatchResults();
      assertEquals(2, results.size());
      BatchResult br = (BatchResult) results.get(0);
      assertEquals(5, br.getUpdateCounts().length);
      br = (BatchResult) results.get(1);
      assertEquals(1, br.getUpdateCounts().length);
    } catch (SQLException e) {
      fail(e.getMessage());
    } finally {
      try {
        sqlMap.endTransaction();
      } catch (SQLException e) {
        fail(e.getMessage());
      }
    }
  }

  public void testExecutebatch() {
    List accountList1 = new ArrayList();
    Account account = new Account();
    account.setId(10);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(11);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(12);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(13);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    account = new Account();
    account.setId(14);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList1.add(account);

    List accountList2 = new ArrayList();
    account = new Account();
    account.setId(15);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    account = new Account();
    account.setId(16);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    account = new Account();
    account.setId(17);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    account = new Account();
    account.setId(18);
    account.setFirstName("fred");
    account.setLastName("flintstone");
    account.setEmailAddress("fred.flintstone@gmail.com");
    accountList2.add(account);

    try {
      sqlMap.startTransaction();
      sqlMap.startBatch();

      // insert 5 accounts
      for (int i = 0; i < accountList1.size(); i++) {
        sqlMap.insert("insertAccountViaInlineParameters", accountList1.get(i));
      }

      // update 1 account
      account = new Account();
      account.setId(10);
      account.setFirstName("barney");
      account.setLastName("rubble");
      account.setEmailAddress("barney.rubble@gmail.com");

      sqlMap.update("updateAccountViaInlineParameters", account);

      // insert 4 accounts
      for (int i = 0; i < accountList2.size(); i++) {
        sqlMap.insert("insertAccountViaInlineParameters", accountList2.get(i));
      }

      int results = sqlMap.executeBatch();
      sqlMap.commitTransaction();

      assertEquals(10, results);
    } catch (SQLException e) {
      fail(e.getMessage());
    } finally {
      try {
        sqlMap.endTransaction();
      } catch (SQLException e) {
        fail(e.getMessage());
      }
    }
  }
}
