package wordsegementation;

public class FSTNumberOrEn {

    final static int numberState = 2;
    final static int englishState = 3;
    final static int startChar = '0';

    int next[];

    public void setTrans(char c, int state) {
        next[c - startChar] = state;
    }

    //英文和中文字符存放进数组中
    public FSTNumberOrEn() {
        next = new int[127];
        for (int i = '0'; i <= '9'; i++) {
            setTrans((char) i, numberState);
        }
        for (int i = 'a'; i <= 'z'; i++) {
            setTrans((char) i, englishState);
        }
        for (int i = 'A'; i <= 'Z'; i++) {
            setTrans((char) i, englishState);
        }
    }

    public int matchNumberOrEn(String text, int offset) {
        int index = offset;

        while (index < text.length()) {
            char c = text.charAt(index);

            int pos = c - startChar;
            if (pos > next.length || pos < 0) {
                return index;
            }

            int t = next[pos];
            if (t == 0) {
                return index;
            }
            index++;
        }

        return index;
    }


    public static void main(String[] args) {
        FSTNumberOrEn fstNumberOrEn = new FSTNumberOrEn();
        System.out.println(fstNumberOrEn.matchNumberOrEn("2019我是中国人", 0));

    }


}
