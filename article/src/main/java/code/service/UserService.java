package code.service;

import code.entity.User;
import code.mapper.UserMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class UserService {
    //添加日志打印功能
    private static Logger log = LogManager.getLogger(UserService.class);

    @Resource
    private UserMapper userMapper;

    public void save(User user) {
        log.info("保存用户 " + user.toString());
        userMapper.insert(user);
    }


}
