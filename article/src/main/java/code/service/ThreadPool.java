package code.service;

import org.springframework.web.context.request.async.DeferredResult;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * <p>description: </p>
 *
 * @author chenrui
 * @since 2018-12-19
 */
public class ThreadPool {

    //启动线程数 = 【任务执行时间/(任务执行时间-IO等待时间)】*CPU内核数
    private ExecutorService fixedExecutorService = Executors.newFixedThreadPool(20);

    public void handle(DeferredResult deferredResult , Callable callback){
        fixedExecutorService.submit(callback);
        deferredResult.onTimeout(new Runnable() {
            @Override
            public void run() {
                deferredResult.setResult("失败");
            }
        });
    }

}
