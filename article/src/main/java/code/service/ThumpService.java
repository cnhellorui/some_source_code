package code.service;

import code.entity.SingerSongName;
import org.springframework.stereotype.Service;

/**
 * <p>description: 批量处理请求的类</p>
 *
 * @author chenrui
 * @since 2018-12-19
 */
@Service
public class ThumpService {

    /**
     * service  ---> buffer ----> dao
     *          <---
     *
     *          需要一个util循环监听这个util，当一些操作过的缓存，然后保存在库中，然后从缓存中删去。
     *
     */



    //获取前台数据

    //LRU数据

    //批量修改

    //在这批修改完的数据，然后保存在库中

    //开启线程去执行操作

}
