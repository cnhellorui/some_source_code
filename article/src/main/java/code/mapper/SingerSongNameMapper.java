package code.mapper;

import code.entity.SingerSongName;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SingerSongNameMapper {

    List<SingerSongName> findSingerSongName(int pagesize, int page);

    void updateSingerSongName(SingerSongName singerSongName);
}
