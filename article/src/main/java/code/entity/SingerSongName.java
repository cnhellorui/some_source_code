package code.entity;


public class SingerSongName {
    private String id;
    private String singerName;
    private String songName;
    private String record;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSingerName() {
        return singerName;
    }

    public void setSingerName(String singerName) {
        this.singerName = singerName;
    }

    public String getSongName() {
        return songName;
    }

    public void setSongName(String songName) {
        this.songName = songName;
    }

    public String getRecord() {
        return record;
    }

    public void setRecord(String record) {
        this.record = record;
    }

    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(" id: " + id).append(" singerName: " + singerName).append(" songName: " + songName).append(" record: " + record);
        return stringBuffer.toString();
    }
}
