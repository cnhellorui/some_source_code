package code.util;

public class HttpResult {
	
	/** http的code */
	private int status_code = 0;
	/** 文本内容 */
	private String content = "";
	
	
	/**
	 * 返回 http的code
	 * @return status_code http的code
	 */
	public int getStatus_code() {
		return status_code;
	}
	/**
	 * 设置 http的code
	 * @param status_code http的code
	 */
	public void setStatus_code(int status_code) {
		this.status_code = status_code;
	}
	/**
	 * 返回 文本内容
	 * @return content 文本内容
	 */
	public String getContent() {
		return content;
	}
	/**
	 * 设置 文本内容
	 * @param content 文本内容
	 */
	public void setContent(String content) {
		this.content = content;
	}


	
}
