package code;

import code.entity.SingerSongName;
import code.entity.User;
import code.mapper.SingerSongNameMapper;
import code.mapper.UserMapper;
import code.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CodeApplicationTests {

	@Autowired
	private UserMapper userMapper;

	@Resource
	private UserService userService;

	@Resource
	private SingerSongNameMapper singerSongNameMapper;

	@Test
	public void contextLoads() {
	}

	@Test
	public void testMapper() {
		User user = new User();
		user.setPhone("123");
		user.setPassword("123111");
		user.setUserName("chenrui");
		userService.save(user);
	}

	@Test
	public void testInsertMany() {

		List<User> list = new ArrayList<>();

		User user = new User();
		user.setPhone("1");
		user.setPassword("123111");
		user.setUserName("hahahaha");

		User user1 = new User();
		user1.setPhone("2");
		user1.setPassword("345678");
		user1.setUserName("houhou");

		list.add(user);
		list.add(user1);

		userMapper.insertUserMany(list);
	}

	@Test
	public void test() {
		long start = System.currentTimeMillis();
		List<SingerSongName> list = singerSongNameMapper.findSingerSongName(0, 0);
		SingerSongName entity = new SingerSongName();
		entity.setRecord("101");
		entity.setSongName("song_name");
		singerSongNameMapper.updateSingerSongName(entity);
		long end = System.currentTimeMillis();
		System.out.println(end - start);
	}
}
