package code.util;

import org.apache.commons.lang.StringUtils;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 目前功能支持情况
 * 属性必须是private，
 * 多行注释
 * <p>
 * 将带有注释的实体bean转成属性：注释
 * 比如
 */

///**
// * 名称
// */
//private String name;

// 变成 map.put("name":"名称")

public class CommentToMap {

    private static final Pattern PROGRAM_REG = Pattern.compile("private\\s[A-Za-z].*\\s([A-Za-z0-9]+)\\;");
    private static final Pattern COMMENT_REG = Pattern.compile("\\/\\*+(.*)\\*\\/");
    private static final Pattern CLASS_REG = Pattern.compile("\\sclass\\s");

    private Queue<String> pragramQueue = new LinkedList<>();

    //读取文件
    public void readfile(String path) {
        try (
                InputStream inputStream = new FileInputStream(path);
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        ) {
            String line = null;
            StringBuilder moreComment = new StringBuilder();
            boolean startBool = false;
            while ((line = reader.readLine()) != null) {
                if (StringUtils.isBlank(line.trim())) {
                    continue;
                }
                line = line.trim();

                //不处理class类之前的注释
                Matcher clazzMather = CLASS_REG.matcher(line);
                if (clazzMather.find()) {
                    startBool = true;
                }
                if (!startBool) {
                    continue;
                }

                //处理多行
                if (line.contains("*")) {
                    moreComment.append(line);
                }
                if (line.contains("*/")) {
                    String programComment = moreComment.toString().replaceAll("\\s", "");

                    Matcher commentMatcher = COMMENT_REG.matcher(programComment);
                    if (commentMatcher.find()) {
                        programComment = commentMatcher.group(1);
                    }

                    //入栈
                    pragramQueue.add(programComment);
                    moreComment.delete(0, moreComment.length());
                    continue;
                }

                //处理单行
                if (line.contains("//")) {
                    moreComment.append(line);
                    pragramQueue.add(moreComment.toString().replace("//", ""));
                    moreComment.delete(0, moreComment.length());
                    continue;
                }

                //处理属性与注释结合
                Matcher programMatcher = PROGRAM_REG.matcher(line);
                if (programMatcher.find()) {
                    //出栈
                    String comment = pragramQueue.poll();
                    if (StringUtils.isBlank(comment)) {
                        continue;
                    }
                    comment = comment.trim();

                    String cleanField = programMatcher.group(1);
                    //打印结果
                    System.out.println("map.put(\"" + cleanField + "\", \"" + comment + "\");");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        new CommentToMap().readfile("D:\\workproject\\User.java");
    }

}
