package code.controller;

import code.dto.WebResult;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>description: 查看当前编译包的标记</p>
 *
 * @author chenrui
 * @date 2018-04-15
 */
@RestController
public class ApiVersionController {

    @Value("${server.version}")
    String serverApiVersion;

    @RequestMapping(value = "serverApiVersion")
    public Object serverApiVersion() {
        return WebResult.getSuccWebResult("0", serverApiVersion);
    }

}
