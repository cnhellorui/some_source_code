/**
 * Author: Paul Reiners
 */
package com.marsdl.ibm;

/**
 * @author Paul Reiners
 * <p>
 * Based closely on code by Andreas Dr&auml;ger
 */
public class BioJavaSample {

    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        LongestCommonSubsequence longestCommonSubsequence = new LongestCommonSubsequence("GCCCTAGCG", "GCGCAATG");
        String longCommon = longestCommonSubsequence.getLongestCommonSubsequence();
        longestCommonSubsequence.printScoreTable();
        System.out.println(longCommon);
    }
}
