import Vue from 'vue'
import Router from 'vue-router'
import TextDiff from "@/components/TextDiff"
import VueUI from "@/components/vueui"

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'text diff',
            component: TextDiff,
        }, 
        {
            path:"/VueUI",
            name:"VueUI",
            component: VueUI
        }
    ]
});