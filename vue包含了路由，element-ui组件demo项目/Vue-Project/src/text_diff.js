// var leftArea = "";
// var rightArea = "";



const textDiff = function(leftAreaText, rightAreaText) {
    // console.log(leftAreaText, rightAreaText);

    if (!leftAreaText || !rightAreaText) {
        console.log("数组存在问题");
        return;
    }
    var letfNum = leftAreaText.length;
    var rightNum = rightAreaText.length;

    var scoreTable = [];

};


function initialize(yAxis, xAxis) {

    var scoreTable = [];
    var yAxisLength = yAxis.length;
    var xAxisLength = xAxis.length;

    for (var y = 0; y < yAxisLength + 1; y++) {
        scoreTable[y] = [];
        for (var x = 0; x < xAxisLength + 1; x++) {

            var cell = {};
            cell.score = 0;
            cell.preCell = null;

            scoreTable[y][x] = cell;
        }
    }
    return scoreTable;
};

function lcsObj(yAxis, xAxis, scoreTable) {

    for (y = 1; y < scoreTable.length; y++) {
        for (x = 1; x < scoreTable[y].length; x++) {
            var currentCell = scoreTable[y][x];
            var cellAbove = scoreTable[y - 1][x];
            var cellToLeft = scoreTable[y][x - 1];
            var cellAboveLeft = scoreTable[y - 1][x - 1];

        }
    }

}


function fillIn(yAxis, xAxis, currentCell, cellAbove, cellAboveLeft, scoreTable) {

}


//最长公共子序列
function LCS(leftAreaText, rightAreaText) {
    if (!leftAreaText && !rightAreaText) {
        return;
    }

    var rows = leftAreaText.split("");
    rows.unshift("");
    var cols = rightAreaText.split("");
    cols.unshift("");
    var rowsNum = rows.length;
    var colsNum = cols.length;
    var dp = []
    for (var i = 0; i < rowsNum; i++) {
        dp[i] = []
        for (var j = 0; j < colsNum; j++) {
            if (i === 0 || j === 0) {
                dp[i][j] = 0;
                continue;
            }

            if (rows[i] === cols[j]) {
                dp[i][j] = dp[i - 1][j - 1] + 1; //对角＋1
            } else {
                dp[i][j] = Math.max(dp[i - 1][j], dp[i][j - 1]); //对左边，上边取最大
            }
        }
        console.log(dp[i].join("")); //调试
    }

    // traceBack(dp, rows, cols);
    return dp[i - 1][j - 1]
}


function walkScoreTable(scoreTable) {
    for (var y = 0; y < scoreTable.length; y++) {
        var arr = [];
        for (var x = 0; x < scoreTable[y].length; x++) {
            arr[x] = scoreTable[y][x].score;
        }
        console.log(arr.join(""));
    }
    console.log(JSON.stringify(scoreTable));
}

function test() {
    var scoreTable = initialize("GCGCAATG", "GCCCTAGCG");
    walkScoreTable(scoreTable);
}

// LCS("GCGCAATG", "GCCCTAGCG");

test();