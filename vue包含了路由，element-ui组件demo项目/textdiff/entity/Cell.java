package com.marsdl.textdiff.entity;

public class Cell {

    private Cell prevCell;
    private int score;
    private int xAxis;
    private int yAxis;

    public Cell() {

    }

    public Cell(int yAxis, int xAxis) {
        this.yAxis = yAxis;
        this.xAxis = xAxis;
    }

    public Cell getPrevCell() {
        return prevCell;
    }

    public void setPrevCell(Cell prevCell) {
        this.prevCell = prevCell;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getxAxis() {
        return xAxis;
    }

    public void setxAxis(int xAxis) {
        this.xAxis = xAxis;
    }

    public int getyAxis() {
        return yAxis;
    }

    public void setyAxis(int yAxis) {
        this.yAxis = yAxis;
    }
}
