package com.marsdl.textdiff;

import com.marsdl.textdiff.entity.Cell;

public class DynamicProgramming {



    protected void initialize(Cell[][] scoreTable) {
        for (int y = 0; y < scoreTable.length; y++) {
            for (int x = 0; x < scoreTable[y].length; x++) {
                scoreTable[y][x] = new Cell(y, x);
            }
        }
        initializeScores(scoreTable);
        initializePointers(scoreTable);
    }

    private void initializeScores(Cell[][] scoreTable) {
        for (Cell[] scoreTableYAxis : scoreTable) {
            for (Cell scoreTableXAxis : scoreTableYAxis) {
                scoreTableXAxis.setScore(0);
            }
        }
    }

    private void initializePointers(Cell[][] scoreTable) {
        for (int y = 0; y < scoreTable.length; y++) {
            for (int x = 0; x < scoreTable[y].length; x++) {
                scoreTable[y][x].setPrevCell(getInitialPointer(y, x, scoreTable));
            }
        }
    }

    private Cell getInitialPointer(int y, int x, Cell[][] scoreTable) {
        if (y == 0 && x != 0) {
            System.out.println(y + "," + x + " => " + y + ", " + (x - 1));
            return scoreTable[y][x - 1];
        } else if (x == 0 && y != 0) {
            System.out.println(y + "," + x + " => " + (y - 1) + ", " + x);
            return scoreTable[y - 1][x];
        } else {
            return null;
        }
    }

}
