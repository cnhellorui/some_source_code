#ifndef _RED_BLACK_TREE_H_
#define _RED_BLACK_TREE_H_

#define RED 0   //红色结点
#define BLACK 1 //黑色结点

typedef int type;

typedef struct rbtree_node
{
    unsigned char color;
    type key;
    struct rbtree_node * left;
    struct rbtree_node * right;
    struct rbtree_node * parent;
}node, * rbtree;


typedef struct rb_root
{
    node * node;
}rbroot;

//创建红黑树
rbroot * create_rbtree();
//销毁红黑树
void destroy_rbtree(rbroot * root);

//将结点插入到红黑树中，插入成功，返回0，失败返回-1 重点掌握，非常重要
int insert_rbtree(rbroot * root, type type);
//删除结点(key为结点的值)，重点掌握，非常重要
void delete_rbtree(rbroot * root, type key);

//前序遍历结点
void preorder_rbtree(rbroot * root);
//中序遍历结点
void inorder_rbtree(rbroot * root);
//后序遍历
void postorder_rbtree(rbroot * root);

//递归实现 红黑树中查找结点
int rbtree_search(rbroot * root, type key);
//非递归实现 红黑树中查找结点
int iterate_rbtree_search(rbroot * root, type key);

int rbtree_minium(rbroot * root, int * val);

int rbtree_maximum(rbroot * root, int * val);

node * rbtree_successor(node * x);

//打印红黑树
void print_rbtree(rbroot * root);

#endif //C_CODE_SOURCE_RBTREE_H








