# some_source_code 一些代码代码

#### 项目介绍
记录一些源码文件，不算一个项目，像一个随心笔记。</br>
当自己有新的学习体会，会第一时间在我的博客分享出来，会添加相应的代码进去。然后存放在这里。<br/>
博客地址：https://blog.csdn.net/Hello_Ray
</br></br>

>* <font face="微软雅黑">article是spring和mybatis整合的代码，又做了一些改动。可以clone下来查看。</font>
>* <font face="微软雅黑">fileserver文件是springboot文件配置整合elasticsearch的全部源码文件 https://blog.csdn.net/Hello_Ray/article/details/82153042</font>
>* <font face="微软雅黑"> xiaoka_h5文件夹是https://blog.csdn.net/Hello_Ray/article/details/81772508</font>
>* <font face="微软雅黑">mongo_file_server是mongodb图片服务器的demo https://blog.csdn.net/Hello_Ray/article/details/82658326</font>
>* <font face="微软雅黑">data_structure/binary_search_tree是二叉搜索树的源码 https://blog.csdn.net/Hello_Ray/article/details/82895168</font>
>* <font face="微软雅">spider_yingyongbaoApp目录下是一个并发爬取应用宝的数据，博客地址 https://blog.csdn.net/Hello_Ray/article/details/84565193</font>
>* <font face="微软雅"> mongo_script是自己经常用到的mongodb js处理的脚本</font>
>* <font face="微软雅">some_source_code/gitlab-manual是gitlab用户的手册，细分到了普通和root用户级别，博客地址 https://blog.csdn.net/Hello_Ray/article/details/86487590</font>

#### 温馨提示
java并发爬取应用宝中的全部数据，在article文件夹中已经提交了springboot+mybatis持久化的配置。如果你有兴趣的话，可以将两个结合起来，将爬取的内容持久化到对应的数据库中。 如果感觉零零散散的话，这些任务我后面会整合在一起。<br/>

最近在整理我的红黑树的代码，还有一些汉语分词。整理好了分词和汉语分词，那么就可以将Thread文件夹中的词云整合起来了。然后就是一整套串连起来。<br/>

自己还要做的事情很多，springboot结合shiro权限控制，每天都在用，但是一直没有总结过。待上面的整理完成后，会把这个做好。千里之行始于足下，慢慢来，该有的都会有的。<br/>

添加了gitlab服务器应用手册 <br />

rust文档 https://docs.rust-embedded.org/discovery/index.html


> 如果你感觉到任何问题，可以issue me or email me chenrui@marsdl.com。




